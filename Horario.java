import java.util.Objects;

public class Horario {


    private int hora = 0;
    private int minuto = 0;
    private int segundo = 0;

    public Horario(int hora, int minuto, int segundo) {
        setHora(hora);
        setMinuto(minuto);
        setSegundo(segundo);
    }

    public Horario(int hora, int minuto) {
        setHora(hora);
        setMinuto(minuto);
    }

    public Horario(){
    }

    public int getHora() {
        return hora;
    }

    public void setHora(int hora) {
        if (hora >= 0 && hora <= 23 ){
            this.hora = hora;
        }else {
            throw new IllegalArgumentException("Hora precisa ser entre 0 e 23");
        }
    }

    public int getMinuto() {
        return minuto;
    }

    public void setMinuto(int minuto) {
        if (minuto >= 0 && minuto < 60){
            this.minuto = minuto;
        }else {
            throw new IllegalArgumentException("Minuto precisa ser entre 0 e 59");
        }
    }

    public int getSegundo() {
        return segundo;
    }

    public void setSegundo(int segundo) {
        if (segundo >= 0 && segundo < 60){
            this.segundo = segundo;
        }else {
            throw new IllegalArgumentException("Segundo precisa ser entre 0 e 59");
        }
    }

    @Override
    public String toString() {
        return "Horario{" +
                "hora=" + hora +
                ", minuto=" + minuto +
                ", segundo=" + segundo +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Horario horario = (Horario) o;
        return hora == horario.hora && minuto == horario.minuto && segundo == horario.segundo;
    }

    @Override
    public int hashCode() {
        return Objects.hash(hora, minuto, segundo);
    }

    public void addHora(){
        if((this.hora + 1) < 23) {
            this.hora = this.hora + 1;
        }else{
            this.hora = 0;
        }
    }

    public void addHora(int horaAdd){
        if((this.hora + horaAdd > 0) && (this.hora + horaAdd) < 23) {
            this.hora = this.hora + horaAdd;
        }else{
            this.hora = horaAdd - 23;
        }
    }

    public void addMinuto(int minutoAdd){
        if((this.minuto + minutoAdd) < 60) {
            this.minuto = this.minuto + minutoAdd;
        }else{
            int horaAdd = (this.minuto + minutoAdd) / 60;
            minutoAdd = (this.minuto + minutoAdd) % 60;
            this.minuto = minutoAdd;
            addHora(horaAdd);
        }
    }

    public void addMinuto(){
        if((this.minuto + 1) < 60) {
            this.minuto = this.minuto + 1;
        }else{
            this.minuto = 0;
            addHora();
        }
    }

    public void addSegundo(int segundoAdd){
        if((this.segundo + segundoAdd) < 60){
            this.segundo = this.segundo + segundoAdd;
        }else{
            int minutoAdd = (this.segundo + segundoAdd) / 60;
            segundoAdd = (this.segundo + segundoAdd) % 60;
            this.segundo = segundoAdd;
            addMinuto(minutoAdd);
        }
    }

    public void addSegundo(){
        if((this.segundo + 1) < 60){
            this.segundo = segundo + 1;
        }else{
            this.segundo = 0;
            addMinuto();
        }
    }


}
